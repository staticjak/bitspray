package com.netaporter

// Messages

trait RestMessage


// Bitcoin Model
case class GetBitcoinExchangeRate(currency: String) extends RestMessage
case class BitcoinExchangeRate(askingPrice: Option[BigDecimal]) extends RestMessage

// Domain objects

case class Pet(name: String) {
  def withOwner(owner: Owner) = EnrichedPet(name, owner)
}

case class Owner(name: String)

case class EnrichedPet(name: String, owner: Owner)

case class Error(message: String)

case class Validation(message: String)

// Exceptions

case object PetOverflowException extends Exception("PetOverflowException: OMG. Pets. Everywhere.")
case object InvalidCurrencyException extends Exception("Some weird currency was given")
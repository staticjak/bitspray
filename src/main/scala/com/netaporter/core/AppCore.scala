package com.netaporter.core

import akka.actor.{Actor, ActorRef}
import com.netaporter.clients.CoinbaseClient.GetAsk
import com.netaporter.{BitcoinExchangeRate, GetBitcoinExchangeRate, InvalidCurrencyException}
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.coinbase.CoinbaseExchange
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.Ticker

class GetBitcoinExchangeRateActor(exchangeService: ActorRef) extends Actor{
  val coinbaseExchange = new CoinbaseExchange().getDefaultExchangeSpecification()
  val coinbase = ExchangeFactory.INSTANCE.createExchange(coinbaseExchange)

  override def receive: Receive = {
    case GetBitcoinExchangeRate(c) if c.isEmpty => throw InvalidCurrencyException
    case GetBitcoinExchangeRate(c) => {
      exchangeService ! GetAsk(c)
      context.become(waitingResponse)
    }

    case BitcoinExchangeRate(None) =>
      val pollMarket = coinbase.getPollingMarketDataService
      val ticker: Ticker = pollMarket.getTicker(CurrencyPair.BTC_USD)
      context.parent ! new BitcoinExchangeRate(Option(ticker.getAsk))
  }

  def waitingResponse: Receive = {
    case BitcoinExchangeRate(ask) => {
      context.parent ! BitcoinExchangeRate(ask)
    }
  }
}

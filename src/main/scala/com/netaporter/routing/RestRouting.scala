package com.netaporter.routing

import akka.actor.{Actor, Props}
import com.netaporter._
import com.netaporter.clients.CoinbaseClient
import com.netaporter.core.GetBitcoinExchangeRateActor
import spray.routing.{HttpService, Route}

class RestRouting extends HttpService with Actor with PerRequestCreator {

  implicit def actorRefFactory = context

  def receive = runRoute(route)

  val exchangeService = context.actorOf(Props[CoinbaseClient])

  val route = {
    get {
      path("askingPrice") {
        parameters('currency) { currency =>
          exchangeRate {
            GetBitcoinExchangeRate(currency)
          }
        }
      }
    }
  }

  def exchangeRate(message: RestMessage): Route =
    ctx => perRequest(ctx, Props(new GetBitcoinExchangeRateActor(exchangeService)), message)

}

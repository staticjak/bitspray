package com.netaporter.clients

import akka.actor.Actor
import com.netaporter._
import com.netaporter.clients.CoinbaseClient.GetAsk
import org.knowm.xchange.coinbase.CoinbaseExchange
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.{Exchange, ExchangeFactory}

class CoinbaseClient extends Actor {
  val coinbaseExchange: Exchange = ExchangeFactory.INSTANCE.createExchange(classOf[CoinbaseExchange].getName);
  val marketDataService = coinbaseExchange.getPollingMarketDataService()

  def receive = {
    case GetAsk(c) =>
      sender ! new BitcoinExchangeRate(Some(marketDataService.getTicker(new CurrencyPair(s"BTC/$c")).getAsk))
  }
}

object CoinbaseClient {
  case class GetAsk(currency: String)
}

# bitspray!

This project creates a REST service capable of retrieving Bitcoin statistics from the Coinbase exchange.

### Overview

Our application is made up of three modules:

 * **Application Core** - The `core` module contains the business logic for our application. 
 * **Routing** - The `routing` module contains our spray routing which describes our RESTful endpoints. It also contains
   our `PerRequest` actor which bridges the gap between the `routing` and the `core` modules and contains the piece of
   code this example project aims to demonstrate.
 * **Clients** - Specific implementation for the exchange clients. Currently only coinbase is supported.

### Running

    sbt run

### Successful request

Current request supported:

    GET http://localhost:38080/askingPrice?currency=USD

We get a successful response:

   {
     "askingPrice": 692.24
   } 



### Validation

None yet. I should probably check that the currency we get is valid.

### Failures

Any failures that not handled by the application core can be escalated up to the supervision strategy in our
`PerRequest` actor. The `PerRequest` actor is too generic to recover from any business logic failures, so it will
simply handle all failures by completing the request with an error response. Any request scoped actors in the
application core are stopped by the `PerRequest` actor.
